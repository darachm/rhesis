---
title: "Computer hardware bugs"
date: 2018-01-26
tags: 
    - issues
    - computer
    - thinkpad
---

# t420 fails to boot, CMOS battery

I've had a 2011 t420 thinkpad for a while. I got it used, and it's
been robustly performing. Then I started to get some issues with 
booting. Here's what happened and how to fix it.

Usually it's on and crunching over night, plugged in, 
with brief reboots every few weeks.
On the infrequent times that I tell it to suspend, I started seeing
a behaviour where it would cycle with power light coming on, 
then it would try to boot, fail, and keep cycling ad infinitum.
Lights on the back of the case were on. The way to get out was to
remove the main battery and unplug, wait a bit, then re-power and
try turning it on.

The day of submitting a paper, in the evening after it was submitted
(great timing), I told it to suspend. In the morning, it refused to
turn on. No lights, no nothing, no how. Nothing on the back, tried
different power supplies. Dead as I could tell. 
Taking out main battery, unplugging, and holding power button for
about 2 minutes didn't do it.

The fix was to take the RAM cover off the underside, flip it over,
pry the keyboard towards the back and off, then reach in there with
a screwdriver and get the CMOS battery plug up and off.
After a minute or two, I put it back in, and it started to
boot off of battery power! Then it died. I re-opened, got it wiggled
off, then re-did it, and got it to boot off of battery again
and back to life. 

I ordered a new CMOS battery now to test if it's just an old
battery. However, over the subsequent months it's been running fine.
I still keep the new battery and a screwdriver in the case,
_in case_.

In conclusion, I think the old dead CMOS battery screws up the boot.
Surprisingly contrary to other reports, it didn't have lights on
the back panel that reacted to power supply events, but nevertheless
it did get fixed.

