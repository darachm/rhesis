---
title: "`Makefiles` and `rmarkdown::render` fighting over reproducible research"
date: 2018-06-14
tags: 
  - issues
  - r
  - makefile
  - rmarkdown
---

# The problem

In effort to both provide a reproducible setup for the analysis for
a paper I was writing, and to placate my perfectionism, I tried 
using `Makefiles` to run a set of `rmarkdown::render`-typeset documents,
for all the R code (and more) I used. It was working fine, then we
sent it out for review. Dumbly, I then updated my laptop, which
then updated libraries, which required `tidy` to update, which
was a mess...

First off, the 1.9 version of `rmarkdown::render` drops support
for the usual latex engine, and requires that you use the author's
own custom reboot of the code. So that was a warning sign, but I
just reverted to 1.8 and it seemed to work.

Later, I realized that the `Makefile` was running `rmarkdown::render`
commands, but they weren't doing the same thing they did a few months
ago! I could swear the options were different, but I don't know how
to pull the old documentation short of installing it so I just tried
to debug it.  
It became a multi-hour nightmare of debug testing lots of different
options, with the order of operations and cleaning mattering for the
output in strange ways.

The main error was pandoc, it couldn't find a png it was looking
for during the typsetting, but `rmarkdown::render` thought it had
made it. Specifically, `*2-1.png` was needed but only `*3-1.png` was
in the `*_files` folder.

It ended up being that if you interrupt a `Makefile` run to debug
something, it'll delete some but not all files that aren't protected.
So it'll leave a `*_cache` folder around for `rmarkdown::render` 
to find, and `rmarkdown::render` assumes that means the files are
made, and skip it.

# The solution

Solution was to deep clean and delete everything, then re-run
everything cleanly from the beginning. This won't be robust to
re-runs and failures, but worked once. I also started to
use the `.SECONDARY` directive in the `Makefile`, but didn't spend
much time since I plan on not investing too much time in learning
any more of GNU make (except what's needed to launch `nextflow`).

# An analogy

It's interesting how software projects interact with their 
responsibility to users of the present and the future.
Changes can break it for folks who depend on the software currently
(like `rmarkdown`'s changes above), but software can also neglect its
responsibility to future users and be paralyzed by the requirements
of supporting the past users (the ossified `Makefile`).

I think my issue with these software really highlights these 
processes. And it's _my_ issue because I'm mixing a young and fast
breaking project with a responsible old-timer.

It reminds me of how genomes evolve. There's diversification trying
to provide the raw material for selection, but there's a paralyzing
"omnigenic" effect of the unfathomable complexity of the cell.
I don't know this field, but from what I've heard I think it can 
paralyze a gene in a web of epistatic interactions, where the 
responsibility of maintaining function with interacting components
can make diversification of a gene be detrimental.
Similar to how software can fork, does copy number variation of
genetic material in a genome provide the raw material to license
a gene to freely explore a fitness landscape? 
