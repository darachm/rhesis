---
title: "Wetbench benchwork little bugs and tweaks"
date: 2018-01-26
tags: 
    - benchwork
    - wetbench
---

# Blue VRC

One time when I was doing FISH, I used VRC as a precautionary RNAse 
inhibitor. I used Buffer B to keep the pH right. I would make it in 
50ml, which
means the dibasic potassium phosphate is 0.8ml but I messed up and put
in 1ml, dumb error. That pushed the pH low. 

Turns out you make VRC by adding VOSO<sub>4</sub>, 
which is very blue, and raising the pH until 
it's green. So the acidic buffer turned the VRC very very blue, and
the cells took up the precipitate. So those were blue cells, and
wouldn't wash off. 
Pretty, but I never tested if the cells were good

Turned out they were the middle, informative timepoints of an
control experiment that I always present now. Now I just say that
I didn't capture the middle because I have the start and the end,
and the ones in the middle ... well they turned blue.

# Quench before spinning

I've optimized a protocol of doing a PFA (paraformaldehyde) fixation
before digestion and hybridization (above). I normally quench the
fix with 0.5M final glycine before centrifugation. One time I didn't
because I wanted to test some shortcuts (someone had tried the
protocol recently and found some steps tedious, he was 
looking for where he could trim and automate to get a higher
throughput out of the method).

So I didn't quench before the spin, and I got really bad hybridization
by that I mean low signal, and the cells were tiny. I tried the whole
thing again with technical replicate raw samples, but this time I
quenched before. Large and good hyb.

I think what happened is that centrifuging cells during a fix, before
the quench, compresses the fixed matrix of connections, making
fixable epitopes more accessible to each other by compressing the
searchable space. Haven't tested this well, though.

