---
title: "Notes about using R"
date: 2018-01-26
tags: 
    - r
---

# Plotting greek and italics in R

(~ 2017)

If you do yeast genetics, you've got to get the nomenclature
right. I suppose that means in plots too. So for doing things
in `R` and `ggplot`, here's some ways. This is my understanding
of nomenclature usage, but I've only ever looked at mRNA and
knockouts so I'm probably not using it right with respect to
counting alleles or thinking about dominance.

Genes are capitalized and italic. So to do that in a plot, let's
say the x-axis, you'd add on 
`xlab( expression(paste( italic(GAP1)," mRNA signal" )))`.
Do note that that `paste` seems to not put spaces in, so it looks
like a different `paste` than the one I'm used to --- or different
arguments interally or something.

Mutants are italicized, lower case, and with a 
[Δ](https://unicode-table.com/en/0394/). So put that in the above
string. I don't know if `expression` has a short hand for that,
I wouldn't be suprised.

To save plots with deltas, either save it as PNG (preferred), but
if you have someone who really likes PDFs for their vector graphics
(I assume that's the advantage?), then you need to use cairo.
So for `ggplot`, that's `library(Cairo)` and put `device=cairo_pdf`
in the `ggsave` call. 
[That's from here]( https://stackoverflow.com/questions/28746938/ggsave-losing-unicode-charaters-from-ggplotgridextra).


# `ungroup()` in the `tidyverse`

(~ 2017)

I had used the massive memory machines on the NYU HPC to do some
large `data.frame` `tidy`-ing and `dplyr`-ing into shape, then made
a tiny little dataframe for plotting. I wanted to `sftp` locally
to plot, but when I opened up the `save()`'d file, R complained:

    Error: vars is not a list

Huh. When I went into `str()` it threw what looked like a tibble
but also a lot of indicies. Turns out, I had not `ungroup()`'d it
before saving, and it was saving the grouping in a way that confused
the save/load system, and it was not properly formed in my local
machine. So `ungroup()` when you save! I kinda wish grouping
of tibbles had scope, to prevent this kind of thing.

# `minqa` install failing

(~ 2017)

`lme4` and `minqa` and all that rock. But there's a little bit of
a snag I've observed in trying to install them (for the purposes
of the `variancePartition` package in Bioconductor). Threw some 
errors, but here's the issue with an install of `minqa`:

    g++ -shared -L/usr/lib64/R/lib -Wl,-O1,--sort-common,--as-needed,-z,relro -o minqa.so altmov.o bigden.o biglag.o bobyqa.o bobyqb.o lagmax.o minqa.o newuoa.o newuob.o prelim.o rescue.o trsapp.o trsbox.o trstep.o uobyqa.o uobyqb.o update.o updatebobyqa.o NULL -lgfortran -lm -lquadmath -L/usr/lib64/R/lib -lR
    g++: error: NULL: No such file or directory

So g++ is getting fed a NULL, where's it coming from? To fix this,
I extracted the archive in the `/tmp` folder, edited the
`src/MakeVars` file to comment out the one line there, so

    PKG_LIBS = `$(R_HOME)/bin/Rscript -e "Rcpp:::LdFlags()"`              

That Rcpp call was returning `NULL` and feeding that to g++. So after
that edit, tar-gzip'd it back up, and installed that tgz. Then
it worked fine.

So why `NULL`? I hadn't `Rcpp` installed! why is this not a 
dependency marked for auto-install? 
I dunno. But now you know how to get around it.

