---
title: "the_cloud_is_the_hive"
date: 2019-01-13
draft: true
---

I always figured it'd be weird and interesting when you'd talk to a robot.
It's motion would be a bit jerky, speech a bit rough around the edges, but
it's strange expanded and curtailed (ie different) thinking would be a 
fascinating alien.

But the future is now, and it turns out that you never talk to the robots - just
their human servants.

Organizations have always used systems to organize human labor. Be it a simple
and brutish command from a violent authority, or a subtle coercion through
an elegant structural suggestion, the coercion of humans is a necessary element
of organizing a human population.

Amazon patent for automating workers

Coercion through pay only works if you need the money.
You need the money because you need to pay rent (mostly, food costs are small
in comparison for a homed person).
You need rent because the sherrif will throw you out of the structure if you
don't pay rent.
Rent gets dictated by landlords, and they ask for as much as they think they
can get based on the market.
And so, you've got collective action by the landlords, subsidized by the
violent eviction services of law enforcement, which sets a base tax that you
simply must pay to the landlords in order to live in an area 
(sounding neo-feudal here).

Fortunately, you came come and go freely, but everywhere else is the same.
And so you pick a local lord with the most tolerable taxes and settle down
into working the local area to make your tax/rent money.
