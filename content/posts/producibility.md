---
title: "Reproducibility is producibility - explicit models"
date: 2017-11-18
draft: true
---

<!--
-->

Biology is in a reproducibility crisis - who can me more reproducible
first?

I'm joking, but it's heartening to see researchers focusing a lot of
attention on how to make analyses reproducible between labs.
I've got swept up in this, sort of, but for unconciously different
reasons.

A graphical user's interface with a point and click interface gives
me great anxiety. It's probably the perfectionist inside me, but
how the heck am I going to draw a perfectly straight line on a
diagram? I'd rather type in two coordinates, in something like
\LaTeX.

For me, scripting everything to make it entirely reproducible is
producibility, ie that I ain't going to get it done unless it's
scripted.

# The virtues of explicit content

There's something fundamentally good about making your methods,
models, and analysis explicit. If you can't express it in
words, is it really all there? It reminds me of the quincunx
question, why does the enormous variety of phenotypes that a
system can take, why does this variety get compressed back down
into simpler DNA code every generation?

Is reproducibile science making science more like biology?
The germline of scripts for analysis are differentiated into
particular configurations, then compute an output?

[]()


"It's important to understand the context of what you're doing, sometimes modularily is over engineering" -yvan



    Science is a process by which models are constructed that are consistent with measurements. Each measurement is built upon a series of assumptions to construct and experimental design (that includes an apparatus).
    Similarly, a scientific project is built up on a series of small bits of knowledge. To omit, or neglect, the small stuff early on is to leave the structure on un-tested foundations.
    Similarly, a scientist can only worry about so much before losing it. Without ever having cause to trust the foundations of the project, it is hard to build sturdily on that. Skipping steps, controls, verifications, validations early on makes a rotten core.
