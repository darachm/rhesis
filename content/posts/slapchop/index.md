---
title: "SLAPCHOP - a flexible bioinformatics tool of filtering, extracting, and re-mixing FASTQ reads with regular expressions"
date: 2018-07-17
draft: true
tags:
    - bioinformatics
---

During the course of an investigation into the genetic factors 
contributing to the accelerated degradation of _GAP1_ mRNA,
we researchers had thought it would be a good idea to screen the
entire budding yeast deletion collection for factors involved.
Essentially, we separate out this collection on the phenotype of
interest, and tabulate how many times we saw each mutant, and
presume that if a mutant was enriched that was deleted for a gene
then that gene may be involved.
Say what you will about the assumptions and practice of genetics,
but this is a fairly standard approach to the problem.

What was tricky was the details of implementation. This required
an enrichment scheme that relied of FACS enriching a fluorescent
reporter (mRNA FISH), and then sequencing of DNA barcodes in the
enriched dead population. 
One issue was that we decided we'd change
the technique we were using in order to make use of UMIs, or 
unique-molecular-identifiers, in order to better estimate abundance 
from low-input populations.
This presented some _challenges_ that were 
unforeseen and the magnitude of these are now quite actively 
repressed in my memory. 

Regardless, I woke up in April 2017 with sequencing reads of an
odd design.

barcode

strain 

UMI

Barcode extraction should be easy by position, but 
This necessitated a barcode extraction tool that used the sequence
context to make choices about how to split and re-mix the read
for downstream use.


---
This is a script that takes each read from a big fastq file, and

uses fuzzy regular expressions to look for anchor sequences and 
use those to cut the matching sequence groups into groups. 
It then repeats this for as many actions
as you specify, and then writes a new FASTQ file as specified from
the regex capture groups. 

This is designed as a tool to parse amplicon sequencing data using
more than just fixed positional anchors. By using regular expressions
we can chop barcodes from indeterminate positions, reassemble a 
fastq record from the results, and filter the results based on 
filters of match positions or types of differences.

For a verbose example of a debugging run:

    ./slapchop.py input.fastq output_basename \
        --bite-size 10 --processes 3  \
        --write-report --limit 10000 \
        -o "Sample:  input   > (?P<sample>[ATCG]{5})(?P<fixed1>GTCCACGAGGTC){e<=1}(?P<rest>TCT.*){e<=1}" \
        -o "Strain:  rest    > (?P<tag>TCT){e<=1}(?P<strain>[ATCG]{10,26})CGTACGCTGCAGGTCGAC"  \
        -o "UMITail: rest    > (?P<fixed2>CGTACGCTGCAGGTC)(?<UMItail>GAC[ATCG]G[ATCG]A[ATCG]G[ATCG]G[ATCG]G[ATCG]GAT){s<=2}"  \
        -o "UMI:     UMItail > (GAC(?P<umi1>[ATCG])G(?<umi2>[ATCG])A(?<umi3>[ATCG])G(?<umi4>[ATCG])G(?<umi5>[ATCG])G(?<umi6>[ATCG])G){e<=2}"  \
        --output-seq "strain" \
        --output-id "input.id+'_umi='+umi1.seq+umi2.seq+umi3.seq+ \
            umi4.seq+umi5.seq+umi6.seq+'_sample='+sample.seq" \
        --filter "sample_length == 5 and rest_start >= 16" \
        --verbose --verbose --verbose

That invocation:

    - Takes records from the `input.fastq`
    - Starts three processes that each take bites of 10 records
    - Applies the four operations to cut up the read
    - Writes the full detailed report including json reports for 
        each read, so we limit it to the first 10,000 bytes
        of the file (about 50 records). This is for debugging.
    - Filters the records on having a `sample` barcode of 5 bases 
        and having the `rest` sequence match starting at least past
        index 16 (so base 15 in english).
    - Re-assembles the records that pass this filter, making the ID
        of the fastq record having the original ID plus a UMI 
        sequence and the sample barcode, then the sequence is just
        the match to the strain barcode context. This is suitable for
        feeding into `bwa` for example.
    - We've got three levels of verbosity, so a per-record verbosity
        for debugging purposes.

Then if we like our thresholds we'd re-run, and drop the `--limit`
and `--write-report` flags. This will turn records like this:

    @NB501157:100:H5J5LBGX2:1:11101:10000:6068 1:N:0:
    CTACTGTCCACGAGGTCTCTGCAGATAATACACTGTCACCCGTACGCTGCAGGTCGACCGTAGGAGGGAGATGTG
    +
    AAAAAEEEE/AEE<EEEEEEEEAEEAEEAEEEEE/EEE/EEEEEEEEE/EEEEEEEEEEEEE/EEEEEEEEEEEE

into records like this:

    @NB501157:100:H5J5LBGX2:1:11101:10000:6068_umi=CTGAGA_sample=CTACT
    GCAGATAATACACTGTCACC
    +
    EEAEEAEEAEEEEE/EEE/E

The sample barcode is the first five, the strain barcode starts after
the `TCT`, and the UMI is interspersed downstream. This is modified
yeast BarSeq.

---

This script leans strongly on the work of 
[regex](https://pypi.org/project/regex/)
and
[Biopython](https://pypi.org/project/biopython/).

---

Todo:

- Need to implement benchmarking to understand where it's slow
- Figure out how to speed up, with refactoring and cython maybe
- Needs more tutorial/examples/documentation
- Report generation R scripts, so making summary plots from these 
    report.csv's
- Unit tests on choice examples from the sequencing ??? Maybe the
    examples above. Might be hard with the stochasticity of
    parallelism.

(wanna help?)
