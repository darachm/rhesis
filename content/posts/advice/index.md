---
title: "Advice"
date: 2018-11-20
draft: true
---

I was talking to a wiser researcher about plans and planning, and they lamented
that advice about science practice isn't always well collected.
Here, I'm collecting links and bits of advice that I've heard over the years.
Redundancy with other collections is a feature.

# Planning 

Gavin Sherlock
- Don't think so much about the far future, because if you can't execute on the
    here and now, you'll never finish the project in front of you. Everyone
    wants to work on the far out stuff because it's interesting, but you got
    to get to the future in order to use it.
- 1/3 1/3 1/3 plan execute interpret
- imagine best possible outcomes
- Codifying lab expectations at outset of the relationship helps keep 
    everything clear.


# Backing up

A librarian 
[told me](https://michi-gato.github.io/start.html)
that everything should be backed up according to the 3-2-1 mnemonic.
This means you make three copies, in two locations, with one offsite.
For me this means there's a copy of everything on my laptop, 
`rsync`'d to a local computer with a big harddrive,
this this is `rclone`'d to an institutional google drive account.
I've heard google drive can fail sporadically, but this is what I can afford.

# Notebooks
