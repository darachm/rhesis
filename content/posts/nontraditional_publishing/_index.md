---
title: "Nontraditional_publishing"
date: 2021-01-07T21:31:38-08:00
draft: true
---

            - "[Why scientists should write blogs instead of articles](http://www.rolfhut.nl/2016/07/27/why-scientists-should-write-blogs-instead-of-articles/)
            - "[Why scientists should write blogs instead of articles](http://www.rolfhut.nl/2016/07/27/why-scientists-should-write-blogs-instead-of-articles/)
                <br> 
                By Rolf Hut.
                "
            - "[Slideboards](http://rajlaboratory.blogspot.com/2017/02/introducing-slideboards-tool-for.html)
    
                A slide-deck-with nice commentary on the side, 
                designed to be accessed online without a 
                presenter-human, a la a kiosk/web-poster mode. 
                Great idea, and a different tool than just HTML
                slides.
                [A demo!](https://slideboard.herokuapp.com/sparks/14?slide_no=3&question_no=4)
                "
