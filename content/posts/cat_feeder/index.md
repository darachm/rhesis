---
title: "Cat_feeder"
date: 2018-07-09T13:57:07-04:00
draft: true
---


For the pi, we flashed a recently downloaded version of Ubuntu server
16.04. Pleasantly surprised to find it configured to SSH out of the
box, and took a little bit of fiddling to get it to like wifi.
Configuring using `network-manager` with `nmcli` had the surprising
behaviour of the wifi functional but disconnecting when the ethernet
was unplugged. Configuration with `wpasupplicant` however 
just worked.


We first attempted a design using a scooper.

That didn't work, as the kibble is large with respect to the scooper,
and so would jam.

So we opened up the internet, read around a bit, and found a 
[successful application of a food processing screw auger](https://sites.google.com/site/barleycat1/overview-and-hardware),
along with some helpful advice (open up the feed to the auger 
as much as possible).

Ebay had a cracked meat grinder auger for $6 with shipping, so
we went there.
