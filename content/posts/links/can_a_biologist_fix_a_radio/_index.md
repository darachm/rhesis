---
title: "Can_a_biologist_fix_a_radio"
date: 2021-01-07T21:30:30-08:00
draft: true
---

            - "[Can a biologist fix a radio?](http://dx.doi.org/10.1016/S1535-6108(02)00133-2)
                <br>
                Yuri Lazebnik's commentary uses the example of a 
                simple radio to argue for a more theoretical 
                and mechanistically fundamental understanding of
                biological processes. An argument against genetics,
                imho, but also a call to think about these 
                limitations as we progress forward.
                This is not to say that being
                more technically sophisticated is the answer, but I 
                like the examples of NPR1, Ziv and Schusters's (2017)
                paper with Gresham and Siegal, and Pritchard's 
                omnigene papers to make the point that biology is 
                complicated. Especially in that last one, a lot of 
                folks may feel like this is a case of \"no shucks
                sherlock\", but I think it's important to accept 
                and teach ideas as the paradigm so that we can 
                immediately get everyone to question them further.
                <br><br>
                Pair with Kacser's \"Some aspects...\" 
                "
    
            - "[Advice to a young scientist](http://dx.doi.org/10.1118/1.594826)
    
                Mendawar writes a fun one, especially if you're just
                starting out in the sciences. I really like the 
                description of apparatus construction, the words
                seem arcane but the metaphor is useful in thinking
                about how methods and science interact.
                "
    
            - "[Into the sciences](http://www.librarything.com/work/19756253/book/142780162)
                <br>
                Fredrick Ross is the guy who wrote in a widely 
                circulated blog post: 
                [\"Fuck you, bioinformatics. Eat shit and die.\"](http://madhadron.com/posts/2012-03-26-a-farewell-to-bioinformatics.html)
                However, even when I disagree with Ross I find his
                arguments useful in thinking about the practice of
                science. It's even better when I agree!
                For example, his perspective on how a non-biologist
                gets started in biology sounds like good advice and
                I've tried to use it. 
                I really like the phrase \"science practitioner\".
                How you train is how you fight, and how you speak is
                how you think, so little turns of phrase like this
                can be powerful.
                "
