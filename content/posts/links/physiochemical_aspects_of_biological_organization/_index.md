---
title: "Physiochemical_aspects_of_biological_organization"
date: 2021-01-07T21:29:23-08:00
draft: true
---

            - "[Some physio-chemical aspects of biological organisation](https://books.google.com/books?id=69l1AwAAQBAJ) <span style=\"background:#fdf;\">WARNING CAT PICTURE</span>
                <br>
                This is the appendix to \"The strategy of the 
                genes\", book of essays by Waddington. Kacser 
                contributes the appendix, and for 1957 it's 
                an especially prescient call for integrating 
                biochemical modeling at the level of systems biology.
                The meat of it isn't very useful given our progress
                in abstracting these mechanisms but could be handy
                for someone new to developmental processes.
                <br><br>
                The best parts are the introduction and conclusion,
                and I don't have a scanner so you'll have to be
                satisfied with down-scaled cell phone photos or
                just buy the book yourself.
                <br><br>
                About genetics:
                ![genetics](./kacser_intro_genetics.jpeg)
                Introduction calling for systems biology:
                ![genetics](./kacser_intro_1.jpeg)
                Really good conclusion:
                ![genetics](./kacser_conclusion.jpeg)
                Growing up in biology in the Age of the Sequencer,
                it has felt like high-throughput biology has pushed
                out some valuable reductionist work<sup>1</sup>.
                and that the next step with these technologies is 
                really to integrate the two so that we're not just 
                doing high-throughput screens, we're doing 
                high-throughput _measurements_ with assays that are 
                readily interpretable back to biophysical properties.
                <br><br>
                And here's a picture of Tai with the book.
                The parts that Waddington wrote are pretty good too,
                classic figures. Development. Evolution. Etc.
                ![cat](./tai_strategy_of_the_genes.jpeg)
                <hr>
                <sup>1</sup> To paraphrase the Buggles: [\"sequencing killed the biochemistry star\"](https://www.youtube.com/watch?v=Iwuy4hHO3YQ)
                "
