---
title: "The_logic_of_chance"
date: 2021-01-07T21:29:59-08:00
draft: true
---

            - "[The Logic of Chance](https://www.amazon.com/gp/offer-listing/0133381064/)
                <br> 
                Koonin. This was my introduction to the amazing work folks have
                been doing with tons of microbial genomes to explore the tree
                of life, or rather forests of trees of life. To compare it to
                genetics, it's like \"systems evolutionary biology\", in that its
                in making this comparisons at this higher level of how the whole
                system of genomes has evolved, doing this gives them insight into
                a new perspective on evolutionary biology. Fun read, and it might
                be accessible for non-biologists as well, if my memory serves
                me right.
                <br><br>
                [Here's a better review.](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3400892/)
                "
