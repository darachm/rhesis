---
title: "about"
---

This website is primarily to serve some of the posts about bugs and fixes.
It is arranged by a human who does the science, fixes bikes, finds problems
and tries to fix them. Also a card-carrying yeast geneticist.

<!--
<span style="unicode-bidi:bidi-override;direction:rtl;" garbledygook></span>
-->
